package at.ac.tuwien.bi.gr31;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;

import java.util.HashSet;
import java.util.Set;

import java.util.regex.Pattern;

import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.FloatWritable;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.StringUtils;
import org.json.JSONObject;

@SuppressWarnings("deprecation")
public class Map extends Mapper<LongWritable, Text, Text, FloatWritable> {

	private Text productId = new Text();
	private FloatWritable score = new FloatWritable();

	private String input;

	// HashSets for good and bad words
	private Set<String> goodWords = new HashSet<>();
	private Set<String> badWords = new HashSet<>();

	// Word boundary defined as whitespace-characters-word boundary-whitespace
	private static final Pattern WORD_BOUNDARY = Pattern.compile("\\s*\\b\\s*");

	protected void setup(Mapper<LongWritable, Text, Text, FloatWritable>.Context context)
			throws IOException, InterruptedException {
		// If the input for this mapper is a file reference, read from the
		// referenced file. Otherwise, read from the InputSplit itself.
		if (context.getInputSplit() instanceof FileSplit) {
			this.input = ((FileSplit) context.getInputSplit()).getPath().toString();
		} else {
			this.input = context.getInputSplit().toString();
		}

		// Check for and set boolean runtime variables.
		Configuration config = context.getConfiguration();

		URI[] localPaths = DistributedCache.getCacheFiles(context.getConfiguration());

		int uriCount = 0;

		parsePositive(localPaths[uriCount++]);
		parseNegative(localPaths[uriCount]);
	}

	// Parse the positive words to match and capture during Map phase.
	private void parsePositive(URI goodWordsUri) {
		try {
			BufferedReader fis = new BufferedReader(new FileReader(new File(goodWordsUri.getPath()).getName()));
			String goodWord;
			while ((goodWord = fis.readLine()) != null) {
				goodWords.add(goodWord);
			}
		} catch (IOException ioe) {
			System.err.println("Caught exception parsing cached file '" + goodWords + "' : "
					+ StringUtils.stringifyException(ioe));
		}
	}

	// Parse the negative words to match and capture during Reduce phase.
	private void parseNegative(URI badWordsUri) {
		try {
			BufferedReader fis = new BufferedReader(new FileReader(new File(badWordsUri.getPath()).getName()));
			String badWord;
			while ((badWord = fis.readLine()) != null) {
				badWords.add(badWord);
			}
		} catch (IOException ioe) {
			System.err.println("Caught exception while parsing cached file '" + badWords + "' : "
					+ StringUtils.stringifyException(ioe));
		}
	}

	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		String line = value.toString();

		Text currentWord = new Text();

		int countPos = 0;
		int countNeg = 0;

		String[] split = WORD_BOUNDARY.split(line);
		for (int i = 0; i < split.length; i++) {

			String word = split[i];
			if (word.isEmpty()) {
				continue;
			}

			currentWord = new Text(word);

			// Count positive words
			if (goodWords.contains(word)) {
				countPos += 1;
			}

			// Count negative words
			if (badWords.contains(word)) {
				countNeg += 1;
			}

		}

		JSONObject json = new JSONObject(line);
		String prodId = json.getString("asin");
		productId.set(new Text(prodId));
		float sentiment = (countPos - countNeg) / (countPos + countNeg);
		score.set(sentiment);
		context.write(productId, score);

	}
}