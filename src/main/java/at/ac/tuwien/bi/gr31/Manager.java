package at.ac.tuwien.bi.gr31;

import java.net.URI;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.filecache.DistributedCache;
// Classes for File IO
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;

@SuppressWarnings("deprecation")
public class Manager extends Configured implements Tool {

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Manager(), args);
		System.exit(res);
	}

	// The run method configures and starts the MapReduce job
	public int run(String[] args) throws Exception {
		Job job = Job.getInstance(getConf(), "manager");
		for (int i = 0; i < args.length; i += 1) {
			if ("-pos".equals(args[i])) {
				job.getConfiguration().setBoolean("manager.pos.patterns", true);
				i += 1;
				DistributedCache.addCacheFile(new URI(args[i]), job.getConfiguration());
			}
			if ("-neg".equals(args[i])) {
				job.getConfiguration().setBoolean("manager.neg.patterns", true);
				i += 1;
				DistributedCache.addCacheFile(new URI(args[i]), job.getConfiguration());
			}
		}

		job.setJarByClass(this.getClass());
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(FloatWritable.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(FloatWritable.class);
		int result = job.waitForCompletion(true) ? 0 : 1;

		return result;

	}
}
