package at.ac.tuwien.bi.gr31;

import java.io.IOException;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class Reduce extends Reducer<Text, FloatWritable, Text, FloatWritable> {

	public void reduce(Text key, Iterable<FloatWritable> values, Context context)
			throws IOException, InterruptedException {
		double sum = 0.0;
		int count = 0;

		for (FloatWritable value : values) {
			sum += value.get();
			count++;
		}

		// Write the product id and the sentiment score
		context.write(key, new FloatWritable((float) (sum / count)));

	}
}
